@extends('layouts.master')

@section('content')
  <div class="container">
    <h1>Products</h1>
    <ul>
    @foreach($products as $product)
      <li><a href="{{ route('products.show', $product->id) }}">{{ $product->name }} – ${{$product->price }}</a></li>
    @endforeach
    </ul>
  </div>
@endsection
