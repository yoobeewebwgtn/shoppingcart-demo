@extends('layouts.master')

@section('content')
  <div class="container">
    <h1>{{ $product->name }}</h1>
    <p>${{ $product->price }}</p>
    <p>{{ $product->description }}</p>
    @if(Auth::check())
      {!! Form::open(['route' => 'cart.add', 'class' => 'form-horizontal']) !!}
        {!! Form::hidden('product_id', $product->id) !!}
        <button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add to Cart</button>
      {!! Form::close() !!}
    @endif
  </div>
@endsection
