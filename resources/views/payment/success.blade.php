@extends('layouts.master')

@section('content')
  <div class="container">
    <h1>Payment Recieved</h1>
    <p>Your payment of {{ $order->price }} has been recieved. Thanks</p>
    <a href="{{ route('home') }}">Home</a>
  </div>
@endsection
