@extends('layouts.master')

@section('content')
  <div class="container">
    <h1>Payment Failed</h1>
    <p>Your payment of {{ $order->subtotal() }} failed to complete.</p>
    <p>The payment processor told us this was the reason: {{ $reason }}</p>
    {!! Form::open(['route' => 'pxpay.purchase']) !!}
      <button class="btn btn-success"><span class="glyphicon glyphicon-cart"></span> Try again</button>
    {!! Form::close() !!}
  </div>
@endsection
