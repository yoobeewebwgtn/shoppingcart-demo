<?php

return [

  // The default gateway to use
  'default' => 'pxpay',

  // Add in each gateway here
  'gateways' => [
    'paypal' => [
      'driver'  => 'PayPal_Express',
      'options' => [
        'solutionType'   => '',
        'landingPage'    => '',
        'headerImageUrl' => ''
      ]
    ],
    'pxpay' => [
      'driver' => 'PaymentExpress_PxPay',
      'options' => [
        'username' => 'Natcoll_Dev',
        'password' => 'fb9f7649ebcdcca74f427183b37c6c5fca3db775e1e04e09eadc612ac786c420'
      ]
    ]
  ]

];
